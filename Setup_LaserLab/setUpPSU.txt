setupMail fdachs@cern.ch
addPSU SUB   /dev/ttyUSB0 m 2  15.10 0.020
addPSU PWELL /dev/ttyUSB0 m 1  6.10 0.021 

addPSU DVDD  /dev/ttyUSB1 m 1 1.90 0.600
addPSU LVDD  /dev/ttyUSB1 m 2 1.90 0.300

addPSU AVDD  /dev/ttyUSB2 m 2 1.90 0.600
addPSU IMON2 /dev/ttyUSB2 m 1 1.40 0.005

addPSU PULSE /dev/ttyUSB3 k 1 1.80 0.005
