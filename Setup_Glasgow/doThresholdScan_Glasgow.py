import os
import sys
import time
import importlib
import argparse
import ROOT

thePython="/sw/atlas/LCG_87/Python/2.7.10/x86_64-slc6-gcc62-opt/bin/python "
#thePython="/afs/cern.ch/atlas/project/tdaq/inst/tdaq-common/tdaq-common-02-02-00/installed/share/bin/tdaq_python "

baseFolder=""

parser=argparse.ArgumentParser()
parser.add_argument('-f' ,'--folder',help='output folder',required=True)
parser.add_argument('-p' ,'--prefix',help='prefix for the file',required=True)
parser.add_argument("-OP",'--onlyPlot'    ,help="Only run plotting",action="store_true")
#arser.add_argument('-r' ,'--repli' ,help='number of replication',type=int,default=1)
#arser.add_argument('-t' ,'--window',help='number of aquisition windows',type=int,default=1000)
#arser.add_argument("-OA",'--onlyAnalysis',help="Only run analysis, do not take data",action="store_true")
#arser.add_argument("-OP",'--onlyPlot'    ,help="Only run plotting",action="store_true")
args=parser.parse_args()

os.system("mkdir -p ThScans/"+args.folder)

theCol  =-1
theIMON2=-1

##make sure that the prefix contains COL and IMON2
if "COL" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: 'COL_x__' block .... please fix it"
    print " " 
    sys.exit()
else:
    colS=args.prefix.split("COL_")[1].split("__")[0]
    print " pulsed column: "+colS
    thCol=int(colS)

if "IMON2" not in args.prefix:
    print " "
    print " PREFIX MUST CONTAIN: '__IMON2_x__' block with 'p' as a separator  .... please fix it"
    print " " 
    sys.exit()
else:
    colS=args.prefix.split("IMON2_")[1].split("__")[0]
    print " imon2 value: "+colS
    theIMON2=float(colS.split("p")[0])+float(colS.split("p")[1])/100.
    print " imon2 final: "+str(theIMON2)

if not args.onlyPlot: 
    os.system( "Set_IMON2_Glasgow.py -v "+str(theIMON2) )

minV  = 1200
maxV  = 1680
step  =  10
ithres=  10
nVal= int((maxV-minV)/step)
print nVal
for i in xrange(0,nVal):
    if args.onlyPlot: continue 
    value=float(minV+step*i)
    vlow="%i"%(value)
    print " "
    print "###############"
    print "## Vlow value: "+str(value)
    print "###############"
    Name="ThScan_"+args.prefix+"__VL"+vlow+".root"
    
    os.system( "Set_VLOW_Glasgow.py -v "+str(value/1000) )
    
    ##os.system("python MaltaNtupleMaker_REAL.py --ithr "+str(ithres)+" --vlow "+vlow+" -t 2000 -r 1 -f "+Name+" --pulse True")
    os.system("python MaltaNtupleMaker_Glasgow.py --ithr "+str(ithres)+" --vlow "+vlow+" -t 15000 -r 1 -f "+Name+" --pulse True")
    os.system("mv NtupleFiles/"+Name.replace(".root","_"+str(ithres)+".root")+"  ThScans/"+args.folder)

print " "
if not args.onlyPlot: 
    os.system("Set_VLOW.py -v 1.00")

#################################
##plotting
print " "
command="python MakeScanPlot.py -f ThScans/"+args.folder+" -p "+args.prefix
os.system(command)
