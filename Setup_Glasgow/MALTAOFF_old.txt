#setVoltage PULSE 0.0
disablePSU PULSE

#setVoltage IMON2 0.0
disablePSU IMON2

#setVoltage AVDD 0.0
disablePSU AVDD
#setVoltage REST 0.0
disablePSU REST

#setVoltage DVDD 0.00
disablePSU DVDD

rampVoltage SUB   0.00 1.0

