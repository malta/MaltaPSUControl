setupMail fdachs@cern.ch
addPSU DVDD  /dev/ttyUSB0 m 2 1.90 2.000
addPSU REST  /dev/ttyUSB0 m 1 1.90 1.000
addPSU SUB   /dev/ttyUSB1 k 1 6.10 0.020
addPSU IMON2 /dev/ttyUSB2 k 1 1.40 0.005
addPSU PULSE /dev/ttyUSB3 k 1 1.80 0.005
addPSU AVDD  /dev/ttyUSB4 k 1 1.85 1.000
addPSU PWELL /dev/ttyUSB6 k 1 6.10 0.020
addPSU ICASN /dev/ttyUSB7 k 1 1.90 0.005

