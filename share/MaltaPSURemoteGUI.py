#!/usr/bin/env python
#######################################
#
# MALTA PSU Remote GUI
#
# GUI based on QT to control MALTA PSU
#
# Abhishek.Sharma@cern.ch
# October 2018
########################################

import os,sys
import time
from os import walk
from subprocess import call
from PyQt5 import QtWidgets,QtCore,QtGui
import MaltaPSUClient

setups = {"Climate Chamber":"pcatlidps03.dyndns.cern.ch",
          "Fluorescence Chamber":"pcatlidps01.dyndns.cern.ch",
          "Scintillator Setup":"pcatldros02.dyndns.cern.ch",
          "H6":"",
          "H8":"",
          "DESY":"192.168.200.107"}

class Window(QtWidgets.QMainWindow):    

    def __init__(self):
        super(Window, self).__init__()
        self.setFixedSize(600,400)
        self.setWindowTitle("Malta Remote PSU Controller")
        global setups
        self.setups=setups
        self.home()

    def home(self):

        QtWidgets.QApplication.setStyle("Plastique")
        wid = QtWidgets.QWidget(self)
        self.setCentralWidget(wid)

        self.Setup_label = QtWidgets.QLabel("Setup",wid)
        self.Setup_label.move(23,20)
        self.cSetup = QtWidgets.QComboBox(wid)
        self.cSetup.move(20,40)
        self.cSetup.activated[str].connect(self.changeSetup)
        for setup in sorted(setups):
            self.cSetup.addItem(setup)
            pass

        self.commString_label = QtWidgets.QLabel("Communication String",wid)
        self.commString_label.move(223,20)
        self.commStringField = QtWidgets.QLineEdit(wid)
        self.commStringField.resize(200,20)
        self.commStringField.move(220,42)
 
        self.btnConnect = QtWidgets.QPushButton("Connect",wid)
        self.btnConnect.clicked.connect(self.connect)
        self.btnConnect.resize(self.btnConnect.minimumSizeHint())
        self.btnConnect.move(440,38)

        self.checkBoxRead = QtWidgets.QCheckBox("Read every (s)",wid)
        self.checkBoxRead.resize(160,30)
        self.checkBoxRead.move(370,70)
        self.checkBoxRead.setChecked(False)
        self.checkBoxRead.stateChanged.connect(self.update)

        self.readFreqField = QtWidgets.QLineEdit(wid)
        self.readFreqField.resize(30,20)#self.setVField[psu].minimumSizeHint())
        self.readFreqField.setText("10")
        self.readFreqField.move(495,70)


        self.posY=100
        
        self.lName = QtWidgets.QLabel("Name",wid)
        self.lOutp = QtWidgets.QLabel("Output",wid)
        self.lVset = QtWidgets.QLabel("Vset [V]",wid)
        self.lIlim = QtWidgets.QLabel("Ilim [A]",wid)
        self.lVout = QtWidgets.QLabel("Vout [V]",wid)
        self.lIout = QtWidgets.QLabel("Iout [A]",wid)

        self.lName.move(20+80*0,self.posY)
        self.lOutp.move(20+80*1,self.posY)
        self.lVset.move(20+80*2,self.posY)
        self.lIlim.move(20+80*3,self.posY)
        self.lVout.move(20+80*4,self.posY)
        self.lIout.move(20+80*5,self.posY)
        
        self.tName={}
        self.tOutp={}
        self.tVset={}
        self.tIlim={}
        self.tVmax={}
        self.tVout={}
        self.tIout={}
        self.bRead={}
        self.bWrite={}

        self.psus={}
      
        self.client=MaltaPSUClient.MaltaPSUClient()

        self.show()
        pass
        
    def changeSetup(self,text):
        if not text in self.setups: return
        hostname=self.setups[text]
        self.commStringField.setText(hostname)
        self.statusBar().showMessage("Setup changed")
        pass
    
    def connect(self):
        hostname=self.commStringField.text()
        if hostname=="": return
        self.client.connect(hostname)
        if self.client.checkConnection() == True:
            self.btnConnect.setStyleSheet("background-color: green")
            pass
        else:
            self.btnConnect.setStyleSheet("background-color: red")
            pass
        pass

    def setPsus(self,psus):
        self.psus=psus;
        pass
        
    def build(self):
        
        for psu in self.psus:
            self.tName[psu] = QtGui.QLabel("%s"%psu,wid)
            self.tOutp[psu] = QtGui.QCheckBox(wid)
            self.tVset[psu] = QtGui.QLineEdit(wid)
            self.tIlim[psu] = QtGui.QLineEdit(wid)
            self.tVout[psu] = QtGui.QLineEdit(wid)
            self.tIout[psu] = QtGui.QLineEdit(wid)
            self.bRead[psu] = QtGui.QPushButton("read",wid)
            self.bWrite[psu] = QtGui.QPushButton("write",wid)

            self.tName[psu].move(20+80*0,self.posY+30*iline)
            self.tOutp[psu].move(20+80*1,self.posY+30*iline)
            self.tVset[psu].move(20+80*2,self.posY+30*iline)
            self.tIlim[psu].move(20+80*3,self.posY+30*iline)
            self.tVout[psu].move(20+80*4,self.posY+30*iline)
            self.tIout[psu].move(20+80*5,self.posY+30*iline)
            self.bRead[psu].move(20+80*6,self.posY+30*iline)
            self.bWrite[psu].move(20+80*7,self.posY+30*iline)
            
            self.tVset[psu].resize(50,20)
            self.tIlim[psu].resize(50,20)
            self.tVout[psu].resize(50,20)
            self.tIout[psu].resize(50,20)
            
            self.tOutp[psu].setChecked(False)
            self.tOutp[psu].stateChanged.connect(lambda state, x=psu: self.enable(x))
            self.bRead[psu].clicked.connect(lambda state, x=psu : self.read(x))
            self.bWrite[psu].clicked.connect(lambda state, x=psu : self.write(x))

            pass
        pass

    def enable(self,psu):
        #maybe should ask if we really want to do it
        if self.tOutp[psu].isChecked()==True:
            self.statusBar().showMessage("Enable %s"%psu)
            self.client.enablePSU(psu)
            pass
        else:
            self.statusBar().showMessage("Disable %s"%psu)
            self.client.disablePSU(psu)
            pass
        pass        

    def update(self):
        self.statusBar().showMessage("Update")
        for psu in self.psus:
            self.read(psu)
            pass
        pass

    def isFloat(self,val):
        try:
            float(val)
        except ValueError:
            return False
        return True
    
    def read(self,psu):
        #VSET
        vset=self.client.getSetVoltage(psu)
        if not self.isFloat(vset): self.tVset[psu].setStyleSheet("background:red;")
        else: self.tVset[psu].setStyleSheet("")
        self.tVset[psu].setText("%.3f"%float(vset))
        #VOUT
        vout=self.client.getVoltage(psu)
        if not self.isFloat(vout): self.tVout[psu].setStyleSheet("background:red;")
        else: self.tVout[psu].setStyleSheet("")
        self.tVout[psu].setText("%.3f"%float(vout))
        #ILIM
        ilim=self.client.getCurrentLimit(psu)
        if not self.isFloat(ilim): self.tIlim[psu].setStyleSheet("background:red;")
        else: self.tIlim[psu].setStyleSheet("")
        self.tIlim[psu].setText("%.3f"%float(ilim))
        #IOUT
        iout=self.client.getCurrent(psu)
        if not self.isFloat(iout): self.tIout[psu].setStyleSheet("background:red;")
        else: self.tIout[psu].setStyleSheet("")
        self.tIout[psu].setText("%.3f"%float(iout))

        self.statusBar().showMessage("Updated PSU: %s"%psu)
        pass

    def write(self,psu):
        #VSET
        vset="%s"%self.tVset[psu].text()
        if "-" in vset: vset = vset.replace("-","")
        if not self.isFloat(vset): self.tVset[psu].setStyleSheet("background:red;")
        else:
            self.tVset[psu].setStyleSheet("")
            self.client.setVoltage(vset)
            pass
        #ILIM
        ilim="%s"%self.tIlim[psu].text()
        if "-" in ilim: ilim = ilim.replace("-","")
        if not self.isFloat(ilim): self.tIlim[psu].setStyleSheet("background:red;")
        else:
            self.tIlim[psu].setStyleSheet("")
            self.client.setCurrentLimit(ilim)
            pass
        self.statusBar().showMessage("Set PSU: %s"%psu)
        QtWidgets.QApplication.processEvents()
        pass


    def close_application(self):
        choice = QtWidgets.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtWidgets.QMessageBox.Yes |
                                            QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            print("Quitting...")
            sys.exit()
        else: pass
        pass

def run():
    app = QtWidgets.QApplication(sys.argv)
    GUI = Window()
    sys.exit(app.exec_())

run()

