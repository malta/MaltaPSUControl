#!/usr/bin/env python
#############################################
# Server for MALTA PSU
#
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
#
# October 2018
# January 2020
#
#
#
#
#
# DEPCRECATED FOR MaltaPSURemoteServer.py ????
#
#
#
#
#
##############################################
import os
import sys
import socket
import json
import time
import _thread
import MALTA_PSU

#PSU configurations

#one argument
zeroparameter=["showConnectedPSUs"]

oneparameter=["enablePSU",
              "disablePSU",
              "getVoltage",
              "getCurrent",
              "getVoltageLimit",
              "getCurrentLimit",
              "showConnectedPSUs",
              "getSetVoltage",
              "getSetCurrent",
              "isEnabled",
              "executeTaskList",
              "setupMail",]

#two arguments
twoparameter=["setVoltageLimit",
              "setCurrentLimit",
              "setVoltage"]

#three arguments
threeparameter=["rampVoltage"]

numpar={"enablePSU":1,
        "disablePSU":1,
        "getVoltage":1,
        "getCurrent":1,
        "getVoltageLimit":1,
        "getCurrentLimit":1,
        "showConnectedPSUs":0,
        "getSetVoltage":1,
        "getSetCurrent":1,
        "isEnabled":1,
        "executeTaskList":1,
        "setupMail":1,
        "setVoltageLimit":2,
        "setCurrentLimit":2,
        "setVoltage":2,
        "rampVoltage":3,
        "addPSU":5,
        }

 
client_list=[]
verbose=True
lastComm="Unknown"

def timestamp():
    return time.strftime("%Y-%m-%d %H:%M:%S")

def log(msg):
    print("%s %s" % (timestamp(),msg))
    pass

def flush():
    if output==None: return
    sys.stdout = open(output,'a+')
    pass

def clientTask(psu,client,address):
    log("Start a new client: %s" % address[0])
    while True:
        try:
            log("Wait for request")
            nreq = int(client.recv(8),16)
            log("Recv len: %i" % nreq)
            sreq = client.recv(nreq)
            log("Recv msg: %s" % sreq)
            pass
        except:
            log("Client closed socket")
            break
        
        #reply dictionary
        log("Decode message")
        dreq = json.loads(sreq)
        drep = {}
        drep["con"]="NOK"
        drep["Log"]=""
        
        try:
            if "cmd" in dreq:
                cmd=str(dreq["cmd"])
                if "Ping" == cmd:
                    log("Ping")
                    drep["Reply"]="Pong"
                    pass                    
                elif "Shutdown" == cmd:
                    log("Server exit requested")
                    drep["Reply"]="Server exit requested"
                    cont=False
                    pass
                elif "getClientName" == cmd:
                    drep["Reply"]=clientname
                    pass
                elif "getConnectionsNumber" == cmd:
                    drep["Reply"]=len(client_list)
                    pass
                elif "getLastComm" == cmd:
                    drep["Reply"]=str(getLastComm())
                    pass
                elif "setupMail" == cmd:
                    drep["Reply"]="Yeah"
                    pass
                elif "addPSU" == cmd:
                    data=dreq["data"]
                    name=dreq["PSUName"]
                    drep["Reply"]="PSU already known: %s" % name
                    if not name in psu.addedPSUs:
                        ret=psu.addPSU(name,data[0],data[1],int(data[2]),float(data[3]),float(data[4]))
                        drep["Reply"]="PSU added"
                        pass
                    pass
                else:
                    #PSU methods
                    psu.clearCmdLog()
                    if cmd in zeroparameter:
                        method=getattr(psu, cmd)
                        r=method()
                        drep["Reply"]=r
                        pass
                    elif cmd in oneparameter:
                        method=getattr(psu, cmd)
                        if "PSUName" in dreq:
                            PSUName=dreq["PSUName"]
                            r=method(PSUName)
                            drep["Reply"]=r
                            pass
                        pass
                    elif cmd in twoparameter:
                        method=getattr(psu, cmd)
                        if "PSUName" in dreq and "data" in dreq:
                            PSUName=dreq["PSUName"]
                            data=dreq["data"]
                            printOA(dreq)
                            r=method(PSUName, data)
                            drep["Reply"]=r
                            pass
                        else:
                            print(timestamp(), "Missing args for command: %s" % cmd)
                            pass
                        drep["Reply"]=psu.getCmdLog()
                        pass
                    elif cmd in threeparameter:
                        method=getattr(psu, cmd)
                        if ("PSUName" in dreq
                            and "data" in dreq
                            and "arg" in dreq):
                            print(dreq)
                            PSUName=dreq["PSUName"]
                            data=dreq["data"]
                            arg=dreq["arg"]
                            r=method(PSUName, data, arg)
                            drep["Reply"]=r
                            pass
                        else:
                            log("Missing args for command: %s" % cmd)
                            pass
                        pass
                    else:
                        log("Command not known: %s " % cmd)
                        pass
                    drep["Log"]=psu.getCmdLog()
                    pass
                drep["con"]="OK"
                pass
            pass
        except:
            log("Exception %s" % sys.exc_info()[0])
            drep["con"]="Request could not be processed"
            pass
        try:
            log("Encode message")
            srep = json.dumps(drep)
            nrep = "%08x" % len(srep)
            log("Send len: %s" % nrep)
            client.send(nrep)
            log("Send msg: %s " %srep)
            client.send(srep)
            pass
        except:
            log("Client closed socket")
            break
        global lastComm
        lastComm=timestamp()
        pass

def startSocket():
    #Starting server
    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        sock.bind((socket.gethostname(),9998))
    except:
        log("Address already in use")
        return 0
    sock.listen(1)
    host=socket.gethostname()#.split(".")[0][:-2]
    
    #Starting up MALTA_PSU
    log("Starting Malta PSU Remote server in %s" % host)
    psu=MALTA_PSU.MALTA_PSU()
    
    cont = True
    while cont:
        log("Waiting for a client . . .")
        flush()
        client, address = sock.accept()#will block until a new client connects
        if address[0] not in client_list: client_list.append(address[0])
        _thread.start_new_thread(clientTask,(psu,client,address))
        pass
    pass

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("Malta PSU Remote Server")
    parser.add_argument("-f","--file",help="log output file")
    
    output=None
    errput=None
    
    args=parser.parse_args()

    if args.file:
        try:        
            print("************************************************************")
            print("Server is running...")
            output=args.file
            os.system("mkdir -p %s"%os.path.dirname(output))
            sys.stdout = open(output,'w+')
            sys.stderr = sys.stdout
            pass
        except:
            print("")
            print("Could not create log file!!.")
            print("Output only in screen")
            print("")
            output=None
            pass
        pass
    else:
        print("************************************************************")
        print("Not loging. Server is running...")
        pass
    startSocket()
    pass

print("Have a nice day")
