#!/usr/bin/env python
# ############################################
# MALTA PSU Client 
# Ignacio.Asensi@cern.ch
# Carlos.Solans@cern.ch
##############################################

import os
import sys
import json
import socket 

class MaltaPSUClient():
    
    def __init__(self):
        print("MaltaPSUClient version 1.0")
        self.verbose=False
        self.host=""
        self.port=9998
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(3)
        self.connected=False
        pass
    
    def setVerbose(self, value):
        self.verbose=value
        pass

    def connect(self, host, port=9998):
        self.host=host
        self.port=port
        print("Connecting to server: %s:%i" % (self.host, self.port))
        try:
            self.s.connect((self.host, self.port))
            self.connected=True
            return True
        except:
            print("Could not connect to server")
            self.connected=False
            return False
        pass

    def close(self):
        self.s.close()
        pass

    def sendCmd(self, cmd, PSUName="", data="", arg=""):
        #parse arguments to send with the command
        if PSUName=="":
            req={"cmd":cmd}
            pass
        elif PSUName!="" and data=="":
            req={"cmd":cmd, "PSUName":PSUName}
            pass
        elif PSUName!="" and data!="" and arg=="":
            req={"cmd":cmd, "PSUName":PSUName, "data":data}
            pass
        elif PSUName!="" and data!="" and arg!="":
            req={"cmd":cmd, "PSUName":PSUName, "data":data, "arg":arg}
            pass
                
        #send request
        if self.verbose: print("Reconnect")
        #self.connect(self.host, self.port)
        if self.verbose: print("Encode message")
        s_req = json.dumps(req)
        n_req = "%08x" % len(s_req)
        if self.verbose: print("Send len: %s" % n_req)
        self.s.send(n_req)
        if self.verbose: print("Send msg: %s" % s_req)
        self.s.send(s_req)
        if self.verbose: print("Wait for reply...")
        n_rep = int(self.s.recv(8),16)
        if self.verbose: print("Recv len: %i" % n_rep)
        s_rep = self.s.recv(n_rep)
        if self.verbose: print("Recv msg: %s" % s_rep)
        if self.verbose: print("Decode message")
        rep=json.loads(s_rep)
        if self.verbose: print("Close connection")
        #self.close()
        if "OK" in rep["con"]:
            return rep
        else:
            print("Reply not OK:" % rep)
            return False
        pass
        
    def getClientName(self):
        resp=self.sendCmd("getClientName")
        return resp
    
    def getConnectionsNumber(self):
        resp=self.sendCmd("getConnectionsNumber")
        return resp["Reply"]
        
    def checkConnection(self):
        resp=self.sendCmd("Ping")
        if resp == False: return False
        if resp["Reply"] == "Pong":return True
        return False
    
    def getlastCommunication(self):
        resp=self.sendCmd("getlastCommunication")
        if resp == False: 
            return False
        else:
            return resp["Reply"]
        pass
    
    #getters
    def getVoltage(self, PSUName):
        cmd="getVoltage"
        resp=self.sendCmd(cmd, PSUName)
        if resp == False: return False
        return resp["Reply"]

    def getCurrent(self, PSUName):
        cmd="getCurrent"
        resp=self.sendCmd(cmd, PSUName)
        if resp == False: return False
        return resp["Reply"]

    def getVoltageLimit(self, PSUName):
        cmd="getVoltageLimit"
        resp=self.sendCmd(cmd, PSUName)
        if resp == False: return False
        return resp["Reply"]

    def getSetVoltage(self, PSUName):
        cmd="getSetVoltage"
        resp=self.sendCmd(cmd, PSUName)
        if resp == False: return False
        return resp["Reply"]

    def getCurrentLimit(self, PSUName):
        cmd="getCurrentLimit"
        resp=self.sendCmd(cmd, PSUName)
        if resp == False: return False
        return resp["Reply"]
    
    def showConnectedPSUs(self):
        cmd="showConnectedPSUs"
        resp=self.sendCmd(cmd)
        if resp == False: return []
        return resp["Reply"]
    
    #setters
    def setVoltage(self, PSUName, data):
        cmd="setVoltage"
        resp=self.sendCmd(cmd, PSUName, data)
        if resp == -1: return False
        return True
    
    def setVoltageLimit(self, PSUName, data):
        cmd="setVoltageLimit"
        resp=self.sendCmd(cmd, PSUName, data)
        if resp == -1: return False
        return True
    
    def setCurrent(self, PSUName, data):
        cmd="setCurrent"
        resp=self.sendCmd(cmd, PSUName, data)
        if resp == -1: return False
        return True
    
    def setCurrentLimit(self, PSUName, data):
        cmd="setCurrentLimit"
        resp=self.sendCmd(cmd, PSUName, data)
        if resp == -1: return False
        return True
    #others
    
    def executeTaskList(self, filepath):
        #read file
        allgood=False
        print("Process task list: %s" % filepath)
        try:
            with open(filepath) as f:
                lines=f.readlines()
                pass
            allgood=True
            pass
        except:
            print("ERROR READING FILE")
            allgood=False
            return False
        if allgood:
            commands=[]
            for l in lines:
                if l.startswith("#") or l.startswith("\n"):continue
                l=l.replace("\n","")
                parts=l.split()
                commands.append(parts)
                pass
            for cmd in commands:
                if True: print("process: %s" % cmd)
                if len(cmd)==1: ret=self.sendCmd(cmd[0])
                if len(cmd)==2: ret=self.sendCmd(cmd[0], cmd[1])
                if len(cmd)==3: ret=self.sendCmd(cmd[0], cmd[1], cmd[2])
                if len(cmd)>3:  ret=self.sendCmd(cmd[0], cmd[1], cmd[2:])
                if ret==False: print("Error in command")
                else: print(ret["Log"].strip())
                pass
            pass
        return True
            
            
    
    def rampVoltage(self, PSUName, data, timewindow):
        cmd="rampVoltage"
        resp=self.sendCmd(cmd, PSUName, data, timewindow)
        if resp == -1: return False
        return True
    
    def isEnabled(self, PSUName):
        cmd="isEnabled"
        resp=self.sendCmd(cmd, PSUName)
        if resp["Reply"] == 1:
            return True
        else:
            return False

    def enablePSU(self, PSUName):
        cmd="enablePSU"
        resp=self.sendCmd(cmd, PSUName)
        if resp["Reply"] == 1:
            return True
        else:
            return False
        
    def disablePSU(self, PSUName):
        cmd="disablePSU"
        resp=self.sendCmd(cmd, PSUName)
        if resp["Reply"] == 1:
            return True
        else:
            return False
        pass
    
