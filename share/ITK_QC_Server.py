#!/usr/bin/env python
# server code
# Ignacio.Asensi@cern.ch

import os
import sys
import socket
import json
import time
import _thread



def log(msg):
    print("Server: %s %s" % (time.strftime("%Y-%m-%d %H:%M:%S"),msg))
    pass

def flush():
    if output==None: return
    sys.stdout = open(output,'a+')
    pass 
def startSocket():
    #Starting server
    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        sock.bind((socket.gethostname(),9998))
    except:
        log("Address already in use")
        return 0
    sock.listen(1)
    host=socket.gethostname()
    
    client_list=[]
    cont = True
    while cont:
        log("Waiting for a client . . .")
        flush()
        client, address = sock.accept()#will block until a new client connects
        if address[0] not in client_list: client_list.append(address[0])
        _thread.start_new_thread(clientTask,(client,address))
        pass
    pass
def executeCmd(cmd,reason):
    # do magic here
    log("executeCmd")
    log(cmd)
    log(reason)
    executed=True
    return executed


def clientTask(client,address):
    allGood=False
    verbose=True
    log("Start a new client: %s" % address[0])
    while True:
        try:
            if verbose: log("Wait for request")
            nreq = int(client.recv(8),16)
            if verbose: log("Recv len: %i" % nreq)
            sreq = client.recv(nreq)
            if verbose: log("Recv msg: %s" % sreq)
            pass
        except:
            if verbose: log("Client closed socket")
            break
        
        #reply dictionary
        if verbose: log("Decode message")
        dreq = json.loads(sreq)
        drep = {}
        try:
            if "cmd" in dreq:
                cmd=str(dreq["cmd"])
                if "Ping" == cmd:
                    log("Ping")
                    drep["Reply"]="Pong"
                    allGood=True
                    pass  
                elif "Shutdown" == cmd:
                    log("Server exit requested")
                    drep["Reply"]="Server exit requested"
                    cont=False
                    pass
                elif "reason" in dreq:
                    allGood=executeCmd(dreq["cmd"],dreq["reason"])
                    pass
                pass
                if allGood:
                    drep["Executed"]="Yes"
                else:
                    drep["Executed"]="No"
                srep = json.dumps(drep)
                if verbose: log("Reply  : %s"%srep[:500])
                client.send(srep.encode())
            pass
        except socket.error as e:
            log("Error %s " % e)
            pass
        pass
    pass


if __name__ == "__main__":
    output = None
    startSocket()
