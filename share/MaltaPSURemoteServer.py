#!/usr/bin/env python
# ############################################
# MiniMALTA PSU  control interface using remote   server
#
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
#
# October 2018
# ############################################
import os
import sys
import socket
import json
import uuid
import time
import subprocess
import re
import pwd
import MALTA_PSU

#PSU configurations

#one argument
zeroparameter=["showConnectedPSUs"]

oneparameter=["enablePSU",
              "disablePSU",
              "getVoltage",
              "getCurrent",
              "getVoltageLimit",
              "getCurrentLimit",
              "showConnectedPSUs",
              "getSetVoltage",
              "getSetCurrent",
              "isEnabled",
              "executeTaskList"]

#two arguments
twoparameter=["setVoltageLimit",
              "setCurrentLimit",
              "setVoltage"]

#three arguments
threeparameter=["rampVoltage"]
 
client_list=[]
verbose=True
lastComm="Unknown"

def timestamp():
    return time.strftime("%Y-%m-%d %H:%M:%S")

def flush():
    if output==None: return
    sys.stdout = open(output,'a+')
    pass

def startSocket():
    #Starting server
    sock = socket.socket()
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    try:
        sock.bind((socket.gethostname(),9998))
    except:
        print (timestamp(),"Address already in use")
        return 0
    sock.listen(1)
    host=socket.gethostname()#.split(".")[0][:-2]
    
    #Starting up MALTA_PSU
    print (timestamp(),"Starting Malta PSU Remote server in %s" % host)
    psu=MALTA_PSU.MALTA_PSU()
    #psu.executeTaskList(os.environ["MALTAPSU_SETUP"]+"/setUpPSU.txt")
    
    cont = True
    while cont:
        print (timestamp(),"Waiting for a client . . .")
        flush()
        client, address = sock.accept()#will block until a new client connects
        while True:
            #check connection is OK
            try:
                sreq = ""
                while True:
                    s=client.recv(1000)
                    if len(s)==0: break
                    sreq+=s
                    if s[-1]=="}": break
                    pass
                pass
            except:
                print (timestamp(),"Client receive broke")
                print (sys.exc_info()[0])
                continue
            
            #Evaluate request
            if len(sreq)==0: break
            if verbose == True: print (timestamp(),"Request: %s"% sreq)
            drep={}
            
            #reply dictionary
            dreq = {}
            drep["con"]="NOK"
            try:
                dreq = json.loads(sreq)
                pass
            except:
                print (timestamp(),"Could not decode the request")
                continue
            
            #print client info
            clientname=client.getpeername()[0]
            if clientname not in client_list:
                client_list.append(clientname)
                print (timestamp()," ==> New client %s" % str(clientname))
                pass
            else:
                pass
            try:
                if "cmd" in dreq:
                    cmd=str(dreq["cmd"])
                    print (timestamp(), "Input from: %s. Command:%s" % (clientname, cmd))
                    #CONNECTIVITY REQUESTS
                    if "Ping" == cmd:
                        if verbose == True: print ("=> Ping from %s" % clientname)
                        drep["Reply"]="Pong"
                        pass                    
                    elif "Shutdown" == cmd:
                        print (timestamp(), "Server exit requested")
                        drep["Reply"]="Server exit requested"
                        cont=False
                        pass
                    elif "getClientName" == cmd:
                        drep["Reply"]=clientname
                        pass
                    elif "getConnectionsNumber" == cmd:
                        drep["Reply"]=len(client_list)
                        pass
                    elif "getLastComm" == cmd:
                        drep["Reply"]=str(getLastComm())
                        pass
                    else:
                        #PSU methods
                        psu.clearCmdLog()
                        if cmd in zeroparameter:
                            method=getattr(psu, cmd)
                            r=method()
                            drep["Reply"]=r
                            pass
                        elif cmd in oneparameter:
                            method=getattr(psu, cmd)
                            if "PSUName" in dreq:
                                PSUName=dreq["PSUName"]
                                r=method(PSUName)
                                drep["Reply"]=r
                                pass
                            pass
                        elif cmd in twoparameter:
                            method=getattr(psu, cmd)
                            if "PSUName" in dreq and "data" in dreq:
                                PSUName=dreq["PSUName"]
                                data=dreq["data"]
                                r=method(PSUName, data)
                                drep["Reply"]=r
                                pass
                            else:
                                print(timestamp(), "Missing args for command: %s" % cmd)
                                pass
                            drep["Reply"]=psu.getCmdLog()
                            pass
                        elif cmd in threeparameter:
                            method=getattr(psu, cmd)
                            if ("PSUName" in dreq
                                and "data" in dreq
                                and "arg" in dreq):
                                PSUName=dreq["PSUName"]
                                data=dreq["data"]
                                arg=dreq["arg"]
                                r=method(PSUName, data, arg)
                                drep["Reply"]=r
                                pass
                            else:
                                print(timestamp(), "Missing args for command: %s" % cmd)
                                pass
                            pass
                        else:
                            print ("Command not known: %s " % cmd)
                            pass
                        drep["Log"]=psu.getCmdLog()
                        pass
                    drep["con"]="OK"
                    pass
                pass
            except ex, err:
                print (timestamp(),"MALTA_PSU chrashed?")
                print (ex,err)
                drep["con"]="OK"
                drep["con"]="Request could not be processed"
                pass
            srep = json.dumps(drep)
            if verbose == True: print (timestamp(),"Reply  : %s"%srep[:500])
            client.send(srep)
            global lastComm
            lastComm=timestamp()
            pass
        client.close()
        pass
    pass

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("Malta PSU Remote Server")
    parser.add_argument("-f","--file",help="log output file")
    
    output=None
    errput=None
    
    args=parser.parse_args()

    if args.file:
        try:        
            print ("************************************************************")
            print ("Server is running...")
            output=args.file
            os.system("mkdir -p %s"%os.path.dirname(output))
            sys.stdout = open(output,'w+')
            sys.stderr = sys.stdout
            pass
        except:
            print ("")
            print ("Could not create log file!!.")
            print ("Output only in screen")
            print ("")
            output=None
            pass
        pass
    else:
        print ("************************************************************")
        print ("Not loging. Server is running...")
        pass
    startSocket()
    pass

print("Have a nice day")
