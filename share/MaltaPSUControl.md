# MaltaPSUControl {#PackageMaltaPSUControl}

Package to control power supplies for a MALTA setup.
It was essential for the first version of MALTA that required lowering and rising the analog voltage for configuration.

The main tool of this package is MALTA_PSU.py that requires a setUpPSU.txt file that is loaded each time the tool is executed. This file takes into account the USB port to which the power supply is connected, the model of the power supply (m for TTi), the channel and the voltage and current ratings. The typical contents look like the following:
 
```
addPSU DVDD  /dev/ttyUSB0 m 1  1.85 0.400
addPSU AVDD  /dev/ttyUSB0 m 2  1.85 0.700
addPSU PWELL /dev/ttyUSB1 m 1  6.10 0.050
addPSU SUB   /dev/ttyUSB1 m 2 20.10 0.050
```

## Python scripts

- MALTA_PSU.py: tool to control the power supplies of the MALTA setup
- ThresholdMonitoring.py 
- CurrentMonitoring.py 
- ThresholdMonitoringTTi.py 
- MaltaPSUServer.py
- InvestigatorPSU.py
- ScintiallatorPSU.py

## Python modules

- realtime.py: make root TGraphs that update in realtime
- MaltaPSUClient.py: client for the MaltaPSUServer.py

