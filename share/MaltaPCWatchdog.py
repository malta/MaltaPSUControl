#!/usr/bin/env python
# ############################################
# MaltaPCWatchdog
#
# Carlos.Solans@cern.ch
# Ignacio.Asensi@cern.ch
#
# October 2018
# ############################################
import os
import sys
import time
import alert
import base64
def timestamp():
    return time.strftime("%Y-%m-%d %H:%M:%S")


def sendMail(sender, pw64 , mailto, subject,message):
    try:
        mailAlert = alert.alert(sender,pw64)
        mailAlert.sendMime(mailto,subject,message)
        pass
    except:
        print "Error sending mail!!"
        pass
    pass

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("PC Watchdog")
    #parser.add_argument("-s","--screen",help="output to screen. Default to file on MaltaSW/MaltaPSUControl/logs/", action="store_true")
    parser.add_argument("-i","--ip",help="IP to watch", required=True)
    parser.add_argument("-m","--mailto",help="Mail to be notified", required=True)
    parser.add_argument("-d", "--delay", help="Delay in seconds", default=30)
    sender="adecmos@cern.ch"
    pw64=base64.b64decode("S2lnZUJlbGUxOA==")
    
    output=None
    errput=None
    args=parser.parse_args()
    mailto=args.mailto
    ipwatch=args.ip
    s_delay=args.delay
    try:
        delay=float(s_delay)
        pass
    except:
        delay=30
        pass
    #subject="test"
    #sendMail(sender, pw64, mailto, subject, subject)
    previous_state=False
    current_state=False
    #and then check the response...
    firstRound=True
    try:
        while True:
            response = os.system("ping -c 1 " + ipwatch)
            if response == 0:
                current_state=True
                if firstRound: print ipwatch, 'is up!. Will notify in change'
                pass
            else:
                current_state=False
                if firstRound: print ipwatch, 'is down!. Will notify in change'
                pass
            if current_state != previous_state and firstRound==False:
                if current_state:
                    print "Online!!", timestamp()
                    subject="%s is online!" % ipwatch
                    sendMail(sender, pw64, mailto, subject, subject)
                    pass
                else:
                    print "Offline!!", timestamp()
                    subject="%s is offline!" % ipwatch
                    sendMail(sender, pw64, mailto, subject, subject)
                    pass
                pass
            previous_state=current_state
            firstRound=False
            time.sleep(delay)
            pass
        pass
    except KeyboardInterrupt:
        print('Stopped')
        pass    
    pass
