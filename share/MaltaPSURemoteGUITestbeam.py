#!/usr/bin/env python
#######################################
#
# MALTA PSU Remote GUI
#
# GUI based on QT to control MALTA PSU
#
# Abhishek.Sharma@cern.ch
# October 2018
########################################

import os,sys
import time
import signal
from os import walk
from subprocess import call
from PyQt4 import QtGui,QtCore
import MaltaPSUClient

global setups
setups = {"Climate Chamber":"pcatlidps03.dyndns.cern.ch",
          "Fluorescence Chamber":"pcatlidps01.dyndns.cern.ch",
          "Scintillator Setup":"pcatldros02.dyndns.cern.ch",
          "H6":"pcatlidps07.dyndns.cern.ch",
          "H8":""}

class Window(QtGui.QMainWindow):    

    def __init__(self):
        super(Window, self).__init__()
        self.setFixedSize(570,410)
        self.setWindowTitle("Malta Remote PSU Controller")
        self.checkBoxPSU={}
        self.setVField={}
        self.getVtext={}
        self.getCtext={}
        self.setVLField={}
        self.setCLField={}
        self.psuLabel={}
        self.psus={}
        self.home()

    def home(self):
  
        QtGui.QApplication.setStyle("Plastique")

        wid = QtGui.QWidget(self)
        self.wid=wid
        self.setCentralWidget(wid)

        self.Setup_label = QtGui.QLabel("Setup",wid)
        self.Setup_label.move(23,20)
        self.comboBoxSetup = QtGui.QComboBox(wid)
        for setup in sorted(setups): self.comboBoxSetup.addItem(setup)
        self.comboBoxSetup.move(20,40)
        self.comboBoxSetup.activated[str].connect(self.setup_choice)

        self.commString_label = QtGui.QLabel("Communication String",wid)
        self.commString_label.move(223,20)
        self.commStringField = QtGui.QLineEdit(wid)
        self.commStringField.resize(200,20)
        self.commStringField.move(220,42)
 
        self.btnConnect = QtGui.QPushButton("Connect",wid)
        self.btnConnect.clicked.connect(self.connect)
        self.btnConnect.resize(self.btnConnect.minimumSizeHint())
        self.btnConnect.move(440,38)

        labelY=90
        self.Enable1_label = QtGui.QLabel("Enable",wid)
        self.Enable1_label.move(80,labelY-15)
        self.Enable2_label = QtGui.QLabel("Output",wid)
        self.Enable2_label.move(80,labelY)
        self.SetVoltage_label = QtGui.QLabel("Set Voltage [V]",wid)
        self.SetVoltage_label.move(145,labelY)
        self.SetVoltageLimit1_label = QtGui.QLabel("Set Voltage",wid)
        self.SetVoltageLimit1_label.move(235,labelY-15)
        self.SetVoltageLimit2_label = QtGui.QLabel("Limit [V]",wid)
        self.SetVoltageLimit2_label.move(235+20,labelY)
        self.SetCurrentLimit1_label = QtGui.QLabel("Set Current",wid)
        self.SetCurrentLimit1_label.move(320,labelY-15)
        self.SetCurrentLimit2_label = QtGui.QLabel("Limit [A]",wid)
        self.SetCurrentLimit2_label.move(320+20,labelY)
        self.Voltage_label = QtGui.QLabel("Voltage [V]",wid)
        self.Voltage_label.move(410,labelY)
        self.Current_label = QtGui.QLabel("Current [A]",wid)
        self.Current_label.move(483,labelY)

        
        
        
        self.btnMaltaON = QtGui.QPushButton("Malta ON",wid)
        self.btnMaltaON.clicked.connect(lambda state, taskname="MALTAON.txt" : self.executeTaskList(taskname))
        #connect(lambda state, x=psu: self.enable(x))
        self.btnMaltaON.resize(self.btnMaltaON.minimumSizeHint())
        self.btnMaltaON.move(20,330)
        
        self.btnMaltaOFF = QtGui.QPushButton("Malta OFF",wid)
        self.btnMaltaOFF.clicked.connect(lambda state, taskname="MALTAOFF.txt" : self.executeTaskList(taskname))
        self.btnMaltaOFF.resize(self.btnMaltaOFF.minimumSizeHint())
        self.btnMaltaOFF.move(110,330)
        
        
        self.btnSet = QtGui.QPushButton("Set",wid)
        self.btnSet.clicked.connect(self.setAll)
        self.btnSet.resize(self.btnSet.minimumSizeHint())
        self.btnSet.move(145,290)

        self.btnRead = QtGui.QPushButton("Read",wid)
        self.btnRead.clicked.connect(self.getAllVnI)
        self.btnRead.resize(self.btnRead.minimumSizeHint())
        self.btnRead.move(235,290)

        self.checkBoxRead = QtGui.QCheckBox("Read every (s)",wid)
        self.checkBoxRead.resize(160,30)
        self.checkBoxRead.move(370,290)
        self.checkBoxRead.setChecked(False)
        self.checkBoxRead.stateChanged.connect(self.readAll)

        self.readFreqField = QtGui.QLineEdit(wid)
        self.readFreqField.resize(30,20)#self.setVField[psu].minimumSizeHint())
        self.readFreqField.setText("10")
        self.readFreqField.move(495,290)


        #self.btnQ = QtGui.QPushButton("Quit",wid)
        #self.btnQ.clicked.connect(self.close_application)
        #self.btnQ.resize(self.btnQ.minimumSizeHint())
        #self.btnQ.move(440,275)

        self.statusField = QtGui.QLineEdit(wid)
        self.statusField.resize(530,30)
        self.statusField.move(10,370)

        self.connection=MaltaPSUClient.MaltaPSUClient()
        done = False
        for setup in sorted(setups):
            if done == True: break
            if self.connection.connect(setups[setup],9998)==True:
                self.hostname = setups[setup]
                index = self.comboBoxSetup.findText(setup,QtCore.Qt.MatchFixedString)
                self.comboBoxSetup.setCurrentIndex(index)
                self.statusField.setText("Connected to %s"%setups[setup])
                done = True
                self.commStringField.setText(setups[setup])
                self.connect()
                pass
            pass

        QtGui.QApplication.processEvents()
        
        self.show()
        pass
        
    def setup_choice(self,text):
        global setups
        self.hostname=setups["%s"%text]
        self.commStringField.setText(self.hostname)
        self.connect()
        
    def connect(self):
        self.hostname=self.commStringField.text()
        if self.hostname!="": self.connection.connect(self.hostname,9998)
        if self.connection.checkConnection() == True:
            self.btnConnect.setStyleSheet("background-color: green")
            self.statusField.setText("Connected to %s"%self.hostname)
            pass
        else:
            self.btnConnect.setStyleSheet("background-color: red")
            self.statusField.setText("Cannot connect to %s"%self.hostname)
            pass

        for psu in self.psus:
            print "Remove PSU: %s" % psu
            self.psuLabel[psu].deleteLater()
            self.setVField[psu].deleteLater()
            self.setVLField[psu].deleteLater()
            self.setCLField[psu].deleteLater()
            self.checkBoxPSU[psu].deleteLater()
            self.getVtext[psu].deleteLater()
            self.getCtext[psu].deleteLater()
            self.psuLabel[psu]=None
            self.setVField[psu]=None
            self.setVLField[psu]=None
            self.setCLField[psu]=None
            self.checkBoxPSU[psu]=None
            self.getVtext[psu]=None
            self.getCtext[psu]=None
            pass
        
        self.psus=self.connection.showConnectedPSUs()

        self.hide()
        QtGui.QApplication.processEvents()
        psuYcounter=0
        wid=self.wid
        for psu in self.psus:
            PSU_labelX = 30
            PSU_labelY = 111
            print "Add PSU: %s" % psu
            
            self.psuLabel[psu] = QtGui.QLabel("%s"%psu,wid)
            self.psuLabel[psu].move(PSU_labelX,PSU_labelY+25*psuYcounter)

            self.setVField[psu] = QtGui.QLineEdit(wid)
            self.setVField[psu].resize(50,20)
            self.setVField[psu].move(155,110+25*psuYcounter)

            self.setVLField[psu] = QtGui.QLineEdit(wid)
            self.setVLField[psu].resize(50,20)
            self.setVLField[psu].move(247,110+25*psuYcounter)
            
            self.setCLField[psu] = QtGui.QLineEdit(wid)
            self.setCLField[psu].resize(50,20)
            self.setCLField[psu].move(330,110+25*psuYcounter)

            self.checkBoxPSU[psu] = QtGui.QCheckBox(wid)
            self.checkBoxPSU[psu].move(95,115+25*psuYcounter)
            self.checkBoxPSU[psu].setChecked(False)
            
            self.checkBoxPSU[psu].stateChanged.connect(lambda state, x=psu: self.enable(x))

            self.getVtext[psu] = QtGui.QLabel("  N/A",wid)
            self.getVtext[psu].resize(40,20)
            self.getVtext[psu].move(410,110+25*psuYcounter)

            self.getCtext[psu] = QtGui.QLabel("  N/A",wid)
            self.getCtext[psu].resize(40,20)
            self.getCtext[psu].move(490,110+25*psuYcounter)

            psuYcounter+=1
            pass

        self.show()
        QtGui.QApplication.processEvents()
                
        self.updatePsus(False)
        pass
    
    def executeTaskList(self, taskname):
        task=os.environ["MALTAPSU_SETUP"]+"/"+taskname
        if not os.path.exists(task):
            self.statusField.setText("Cannot find %s!!" % taskname)
            pass
        else:
            if self.connection.executeTaskList(task) == True:
                self.statusField.setText(taskname)
                pass
            else:
                self.statusField.setText("Error executing %s" % taskname)
                pass
            pass
        self.updatePsus(False)
        pass
    
    def enable(self,psu):
        print "Callback PSU: ", psu
        if self.checkBoxPSU[psu].isChecked()==True:
            self.connection.enablePSU(psu)
            self.statusField.setText("Enabling %s"%psu)        
            pass
        elif self.checkBoxPSU[psu].isChecked()==False:
            self.connection.disablePSU(psu)
            self.statusField.setText("Disabling %s"%psu)
            pass
        pass
    

    def updatePsus(self,monitoring=True):
        for psu in self.psus:
            #print "PSU: %s" % psu
            if not monitoring:
                iena = self.connection.isEnabled(psu)
                self.checkBoxPSU[psu].blockSignals(True)
                self.checkBoxPSU[psu].setChecked(iena)
                self.checkBoxPSU[psu].blockSignals(False)
                vset = self.connection.getSetVoltage(psu)
                self.setVField[psu].setText(str("%.3f"%vset))
                vlim = self.connection.getVoltageLimit(psu)
                self.setVLField[psu].setText(str("%.3f"%vlim))
                clim = self.connection.getCurrentLimit(psu)            
                self.setCLField[psu].setText(str("%.3f"%clim))
                pass
            vout = self.connection.getVoltage(psu)
            self.getVtext[psu].setText(str("%.3f"%vout))
            cout = self.connection.getCurrent(psu)
            self.getCtext[psu].setText(str("%.3f"%cout))
            QtGui.QApplication.processEvents()
            pass
        pass

    def getAllVnI(self):
        self.statusField.setText("Reading values...")
        self.btnRead.setText("Reading...")
        QtGui.QApplication.processEvents()
        self.updatePsus(False)
        self.btnRead.setText("Read")
        self.statusField.setText("Last updated: "+time.strftime("%Y-%m-%d %H:%M:%S"))
        pass
    
    def setAll(self):
        self.statusField.setText("Setting values...")
        QtGui.QApplication.processEvents()
        vsets={}
        vlims={}
        clims={}
        problem=False
        for psu in self.psus:
            print "PSU: %s" % psu
            
            vset = "%s"%self.setVField[psu].text()
            if "-" in vset: vset = vset.replace("-","")
            vsets[psu] = True
            if vset == "": vsets[psu] = False
            for char in vset:
                if char.isalpha() == True or " " in vset: vsets[psu] = False
                pass
            if vsets[psu] == True:
                #self.connection.setVoltage(psu,"%.3f"%float(vset))
                self.connection.rampVoltage(psu,"%.3f"%float(vset))
                self.setVField[psu].setStyleSheet("")
                pass
            else:
                self.setVField[psu].setStyleSheet("background: red;")
                problem=True
                pass
            
            vlim = "%s"%self.setVLField[psu].text()
            if "-" in vlim: vlim = vlim.replace("-","")
            vlims[psu] = True
            if vlim == "": vlims[psu] = False
            for char in vlim:
                if char.isalpha() == True or " " in vlim: vlims[psu] = False
                pass
            if vlims[psu] == True:
                self.connection.setVoltageLimit(psu,"%.3f"%float(vlim))
                self.setVLField[psu].setStyleSheet("")
                pass
            else:
                self.setVLField[psu].setStyleSheet("background: red;")
                problem=True
                pass

            clim = "%s"%self.setCLField[psu].text()
            if "-" in clim: clim = clim.replace("-","")
            clims[psu] = True
            if clim == "": clims[psu] = False
            for char in clim:
                if char.isalpha() == True or " " in clim: clims[psu] = False
                pass
            if clims[psu] == True:
                self.connection.setCurrentLimit(psu,"%.3f"%float(clim))
                self.setCLField[psu].setStyleSheet("")
                pass
            else:
                self.setCLField[psu].setStyleSheet("background: red;")
                problem=True
                pass
            
            pass
        if problem: self.statusField.setText("Appropriate values have been set. Check fields in RED.")
        else:       self.statusField.setText("Values have been set.")
        pass


    def readAll(self):
        interval = int(self.readFreqField.text())
        while self.checkBoxRead.isChecked()==True: 
            self.getAllVnI()
            QtGui.QApplication.processEvents()
            time.sleep(interval)
            pass
        self.statusField.setText("")
        pass

    def close_application(self):
        choice = QtGui.QMessageBox.question(self, "Quitting...",
                                            "Are you sure you wish to quit?",
                                            QtGui.QMessageBox.Yes |
                                            QtGui.QMessageBox.No)
        if choice == QtGui.QMessageBox.Yes:
            print("Quitting...")
            sys.exit()
        else: pass
        pass


def run():
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    app = QtGui.QApplication(sys.argv)
    gui = Window()
    def aboutToQuit():
        return gui.close_application()
        
    #app.aboutToQuit.connect(aboutToQuit)
    sys.exit(app.exec_())

run()

raw_input("")
