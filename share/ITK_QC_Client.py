#!/usr/bin/env python
# client code
# Ignacio.Asensi@cern.ch

import os
import sys
import json
import socket 
import time

class SocketClient():
    
    def __init__(self):
        print("SocketClient version 1.0")
        self.verbose=False
        self.host=""
        self.port=9998
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.settimeout(3)
        self.connected=False
        pass
    
    def setVerbose(self, value):
        self.verbose=value
        pass

    def connect(self, host, port=9998):
        self.host=host
        self.port=port
        print("Connecting to server: %s:%i" % (self.host, self.port))
        try:
            self.s.connect((self.host, self.port))
            self.connected=True
            return True
        except:
            print("Could not connect to server")
            self.connected=False
            return False
        pass
    def setVerbose(self):
        self.verbose=True
        pass
    def close(self):
        self.s.close()
        pass
    
    @staticmethod
    def log(msg):
        print("Server: %s %s" % (time.strftime("%Y-%m-%d %H:%M:%S"),msg))
        pass
    
    def sendCmd(self, cmd, reason=""):
        req={"cmd":cmd, "reason":reason}
        s_req = json.dumps(req)
        n_req = "%08x" % len(s_req)
        if self.verbose: self.log("Send len: %s" % n_req)
        self.s.send(n_req.encode())
        if self.verbose: self.log("Send msg: %s" % s_req)
        self.s.send(s_req.encode())
        try:
            if self.verbose: self.log("Wait for reply...")
            s_rep = self.s.recv(64).decode()
            if self.verbose: self.log("Recv msg: %s" % s_rep)
            if self.verbose: self.log("Decode message")
            rep=json.loads(s_rep)
            self.log("Executed: %s" % rep["Executed"])
            pass
        except socket.error as e:
            self.log("Error %s " % e)
            pass
        if self.verbose: self.log("Close connection")
        pass
    pass

if __name__ == "__main__":
    
    client=SocketClient()
    client.setVerbose()
    client.connect(socket.gethostname())
    
    client.sendCmd("Interlock","Reason: high temp.")
    time.sleep(2)
    client.sendCmd("Rm Interlock","Reason: condition OK")

    client.close()
    print("Done")
        
    
