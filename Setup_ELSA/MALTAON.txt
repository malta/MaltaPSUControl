enablePSU SUB
rampVoltage SUB   6.00 2.0

setVoltage DVDD   1.20
enablePSU  DVDD

setVoltage REST   1.80
disablePSU REST

setVoltage IMON2  1.00
disablePSU IMON2

enablePSU SUB2
rampVoltage SUB2   1.00 2.0

setVoltage DVDD2   1.20
enablePSU  DVDD2

setVoltage REST2   1.80
disablePSU REST2


