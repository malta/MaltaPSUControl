#enablePSU MSUB
#rampVoltage MSUB   6.00 2.0
#setVoltage MDVDD   1.20
#enablePSU  MDVDD
#setVoltage MREST   1.80
#disablePSU MREST
#setVoltage MIMON2  1.00
#disablePSU MIMON2


enablePSU MSUB
enablePSU MPWELL
rampVoltage MSUB   6.00 2.0
rampVoltage MPWELL 6.00 2.0

setVoltage MDVDD   1.20
enablePSU MDVDD

setVoltage MLVDD   1.80
enablePSU MLVDD

disablePSU MAVDD
setVoltage MAVDD   1.80

setVoltage MIMON2  1.00
disablePSU MIMON2
