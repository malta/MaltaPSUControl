enablePSU SUB
enablePSU PWELL
rampVoltage SUB   6.00 2.0
rampVoltage PWELL 6.00 2.0

setVoltage DVDD   1.20
enablePSU DVDD

setVoltage LVDD   1.80
enablePSU LVDD

disablePSU AVDD
setVoltage AVDD   1.80

setVoltage IMON2  1.30
disablePSU IMON2

