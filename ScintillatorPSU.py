#!/usr/bin/env python
import TTi
import Keithley
import time 
import argparse

parser=argparse.ArgumentParser()
parser.add_argument("-on",help="Power Scintillator ON",action='store_true')
parser.add_argument("-off",help="Power Scintillator OFF",action='store_true')

args=parser.parse_args()


#Vplus          = TTi.TTi("/dev/ttyUSB2")
HV            = Keithley.Keithley("/dev/ttyUSB2")

if args.on:
    print "Turn on Scintillator"

    HV.setVoltageLimit(15.1)
    HV.setCurrentLimit(0.02)
    HV.enableOutput(True)
    HV.rampVoltage(15,2,1)
#    HV.setVoltage(15)

    pass

if args.off:
    
    print "Turn off Scintillator"

    HV.rampVoltage(0,2,1)
    HV.enableOutput(False)

    HV.close()
 
    pass
