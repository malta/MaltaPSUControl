disablePSU DVDD
setVoltage DVDD 0.0

disablePSU AVDD
setVoltage AVDD 0.0

rampVoltageDown PWELL 5.00 1.0
rampVoltageDown SUB   5.00 1.0

rampVoltageDown PWELL 4.00 1.0
rampVoltageDown SUB   4.00 1.0

rampVoltageDown PWELL 3.00 1.0
rampVoltageDown SUB   3.00 1.0

rampVoltageDown PWELL 2.00 1.0
rampVoltageDown SUB   2.00 1.0

rampVoltageDown PWELL 1.00 1.0
rampVoltageDown SUB   1.00 1.0

rampVoltageDown PWELL 0.00 1.0
rampVoltageDown SUB   0.00 1.0

disablePSU PWELL


