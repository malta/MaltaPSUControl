#LGAD P3 and P4
addPSU LGAD_amp /dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT43WNAL-if02-port0 m 2 15.0 0.600
addPSU LGAD_LV  /dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT43WNAL-if02-port0 m 1 2.50 0.020
addPSU LGAD_HV  /dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT43WNAL-if03-port0 k 0 -180.00 0.000001

## MALTA PWELL and SUB P2
addPSU PWELL    /dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT43WNAL-if01-port0 m 1   6.10 0.021 
addPSU SUB      /dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT43WNAL-if01-port0 m 2  35.10 0.021 

## MALTA AVDD and DVDD P1
addPSU DVDD  /dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT43WNAL-if00-port0  m 1 1.90 0.600
addPSU AVDD  /dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT43WNAL-if00-port0  m 2 1.90 0.550
