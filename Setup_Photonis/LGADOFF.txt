rampVoltageUp LGAD_HV  -170.00 0.000001
rampVoltageUp LGAD_HV  -160.00 0.000001
rampVoltageUp LGAD_HV  -150.00 0.000001
rampVoltageUp LGAD_HV  -140.00 0.000001
rampVoltageUp LGAD_HV  -130.00 0.000001
rampVoltageUp LGAD_HV  -120.00 0.000001
rampVoltageUp LGAD_HV  -110.00 0.000001
rampVoltageUp LGAD_HV  -100.00 0.000001
rampVoltageUp LGAD_HV  -90.00 0.000001
rampVoltageUp LGAD_HV  -80.00 0.000001
rampVoltageUp LGAD_HV  -70.00 0.000001
rampVoltageUp LGAD_HV  -60.00 0.000001
rampVoltageUp LGAD_HV  -50.00 0.000001
rampVoltageUp LGAD_HV  -40.00 0.000001
rampVoltageUp LGAD_HV  -30.00 0.000001
rampVoltageUp LGAD_HV  -20.00 0.000001
rampVoltageUp LGAD_HV  -10.00 0.000001
rampVoltageUp LGAD_HV  -0.00 0.000001

disablePSU LGAD_HV
disablePSU LGAD_amp
disablePSU LGAD_LV
