enablePSU REF_VCC
rampVoltageUp   REF_VCC	   2.25   1.80

enablePSU REF_SEC
rampVoltageUp	REF_SEC	  12.00   4.80

enablePSU REF_HV
rampVoltageDown REF_HV   -50.00  10.00
rampVoltageDown REF_HV  -100.00  10.00
rampVoltageDown REF_HV  -150.00  10.00
rampVoltageDown REF_HV  -180.00  12.00
