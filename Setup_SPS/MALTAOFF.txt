disablePSU IMON2  
 
disablePSU LVDD 
setVoltage LVDD 0.0 
 
disablePSU DVDD 
setVoltage DVDD 0.0 
 
disablePSU AVDD 
setVoltage AVDD 0.0 

rampVoltage PWELL 5.00 2.0 
rampVoltage SUB   5.00 2.0 
rampVoltage SUBH   5.00 2.0 
 
rampVoltage PWELL 4.00 2.0 
rampVoltage SUB   4.00 2.0 
rampVoltage SUBH   4.00 2.0 
 
rampVoltage PWELL 3.00 2.0 
rampVoltage SUB   3.00 2.0 
rampVoltage SUBH   3.00 2.0 
   
rampVoltage PWELL 2.00 2.0 
rampVoltage SUB   2.00 2.0 
rampVoltage SUBH   2.00 2.0 
 
rampVoltage PWELL 1.00 2.0 
rampVoltage SUB   1.00 2.0 
rampVoltage SUBH   1.00 2.0 
 
rampVoltage PWELL 0.00 2.0 
rampVoltage SUB   0.00 2.0 
rampVoltage SUBH   0.00 2.0 
 
disablePSU PWELL 

