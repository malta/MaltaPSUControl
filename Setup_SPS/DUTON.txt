enablePSU DUT1_SUB
enablePSU DUT_PWELL

rampVoltageUp DUT1_SUB   1.00 2.0
rampVoltageUp DUT_PWELL 1.00 2.0

rampVoltageUp DUT1_SUB   2.00 2.0
rampVoltageUp DUT_PWELL 2.00 2.0

rampVoltageUp DUT1_SUB   3.00 2.0
rampVoltageUp DUT_PWELL 3.00 2.0

rampVoltageUp DUT1_SUB   4.00 2.0
rampVoltageUp DUT_PWELL 4.00 2.0

rampVoltageUp DUT1_SUB   4.50 2.0
rampVoltageUp DUT_PWELL 4.50 2.0

rampVoltageUp DUT1_SUB   5.00 2.0
rampVoltageUp DUT_PWELL 5.00 2.0

rampVoltageUp DUT1_SUB   5.50 2.0
rampVoltageUp DUT_PWELL 5.50 2.0

rampVoltageUp DUT1_SUB   6.00 2.0
rampVoltageUp DUT_PWELL 6.00 2.0

setVoltage DUT_DVDD 1.20
enablePSU DUT_DVDD

disablePSU DUT_AVDD
setVoltage DUT_AVDD 1.80

#
