#!/usr/bin/env python
#############################################
# MALTA PSU Current Monitoring Display
#
# Carlos.Solans@cern.ch
# Abhishek.Sharma@cern.ch
# March 2018
#############################################

import os
import sys
import math
import array
import time
import signal
import argparse
import SerialCom
import realtime
import ROOT
import TTi
import Keithley

parser = argparse.ArgumentParser()
parser.add_argument('-v','--voltage',help='set voltage',type=float,default=0)
parser.add_argument('-c','--compliance',help='set compliance',type=float,default=0)
args = parser.parse_args()

ROOT.gStyle.SetPadLeftMargin(0.14)
ROOT.gStyle.SetPadBottomMargin(0.15)
ROOT.gStyle.SetPadRightMargin(0.15)
ROOT.gStyle.SetPadTopMargin(0.07)
ROOT.gStyle.SetLabelSize(0.07,"x")
ROOT.gStyle.SetLabelSize(0.07,"y")
ROOT.gStyle.SetTitleSize(0.07,"x")
ROOT.gStyle.SetTitleSize(0.07,"y")
ROOT.gStyle.SetTitleOffset(1.10,"y")
ROOT.gStyle.SetOptTitle(0);

cont = True
def signal_handler(signal, frame):
    print "You pressed ctrl+C to quit" 
    global cont
    cont = False
    return

threshold = TTi.TTi("/dev/ttyUSB0")


#c1 = ROOT.TCanvas("c","Current Monitoring", 800,800)
#c1.Divide(1,2,0.001,0.02)
#gC = realtime.Plot("C",";Time [s]; Current [mA]")
#gV = realtime.Plot("V",";Time [s]; Voltage [V]")
#gC.SetVerbose(True)
#gC.SetRangeUser(-40,600)
#gV.SetRangeUser(-1,7)
#PSUs = {Avdd_Dac:["Avdd","Dac"],Dvdd_Lvdd:["Dvdd","Lvdd"],Pwell_Sub:["Pwell","Sub"]}
#count=1
#for ps in PSUs:
#    for output in xrange(2):
#        print "Add plot: %s" % ps
#        gC.AddPlot(PSUs[ps][output],"PL")
#        gV.AddPlot(PSUs[ps][output],"PL")
#        c1.Update()
#        count+=1
#        pass
#    pass
#        
#time0=time.time()

signal.signal(signal.SIGINT, signal_handler)

#threshold.setVoltage(args.voltage)
#threshold.setCurrentLimit(args.compliance)

while True:
    if cont==False: break
    curr1 = threshold.getCurrent(1)
    curr1 = float(curr1[:-1])*1000
    currl1=float(threshold.getCurrentLimit(1)[3:])*1000
    curr2 = threshold.getCurrent(2)
    curr2 = float(curr2[:-1])*1000
    currl2=float(threshold.getCurrentLimit(2)[3:])*1000
    print "current [mA]: %.3f"%curr1,"comp [mA]: %.3f"%currl1,"current [mA]: %.3f"%curr2,"comp [mA]: %.3f"%currl2
    #pass
