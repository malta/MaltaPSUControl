rampVoltage SUB    2.00 1.0
rampVoltage PWELL  0.00 1.0
rampVoltage SUB    0.00 1.0

disablePSU AVDD
setVoltage AVDD 0.0

disablePSU DVDD
setVoltage DVDD 0.0
